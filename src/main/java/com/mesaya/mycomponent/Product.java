/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mesaya.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class Product {

    private int id;
    private String Name;
    private double price;
    private String image;

    public Product(int id, String Name, double price, String image) {
        this.id = id;
        this.Name = Name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Espresso", 40, "1.jpg"));
        list.add(new Product(2, "Americano", 45, "2.jpg"));
        list.add(new Product(3, "Cappuccino", 40, "3.jpg"));
        list.add(new Product(4, "Latte", 45, "4.jpg"));
        list.add(new Product(5, "Macchiato", 50, "5.jpg"));
        list.add(new Product(1, "Vanilla Latte", 60, "6.jpg"));
        list.add(new Product(1, "Mocha", 55, "7.jpg"));
        list.add(new Product(1, "Hazelnut Latte", 75, "8.jpg"));
        list.add(new Product(1, "Chocolate Milk", 80, "9.jpg"));
        list.add(new Product(1, "Cortado", 70, "10.jpg"));
        return list;
    }
}


